plotly==4.6.0
matplotlib==3.2.1
numpy==1.18.4
python-telegram-bot==12.8
beautifulsoup4==4.8.1
pydash==4.8.0
requests==2.24.0
peewee==3.13.3
arrow==0.16.0
requests==2.24.0
libvoikko==4.3
nltk==3.5
