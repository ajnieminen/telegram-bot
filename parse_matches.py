from requests import get
from bs4 import BeautifulSoup
from datetime import datetime

def get_matches():

    URL = 'https://www.hltv.org/matches'
    page = get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')

    matches = soup.find_all('div', class_='upcomingMatch')

    for match in matches:

        if 'team1' not in match.attrs:
            continue

        unix_timestamp = int(match.find('div', class_='matchTime')['data-unix'])
        dt = datetime.fromtimestamp(unix_timestamp / 1000)
        team_names = match.find_all('div', class_='matchTeamName')
        url = match.find('a', class_='match')['href']
        meta = match.find('div', class_='matchMeta').getText()

        yield {
            'id': int(url.split('/')[2]),
            'url': url,
            'datetime': dt,
            'meta': meta,
            'team_ids': [match['team1'], match['team2']],
            'team_names': [n.get_text() for n in team_names]
        }

for m in get_matches():
    print(m)
