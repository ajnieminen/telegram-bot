#!/usr/bin/env python

from glob import glob
from importlib import import_module


def run_cron():
    for f in glob('scripts/script_*.py'):
        module_name = f.replace('/', '.').replace('.py', '')
        module = import_module(module_name, package=__name__)
        try:
            module.cron()
        except Exception:
            pass


if __name__ == "__main__":
    run_cron()
