#!/usr/bin/env python3

import json
from os import listdir, environ
from importlib import import_module
from glob import glob
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from pydash import get
import requests


token = environ['tbot_api']

updater = Updater(token, use_context=True)

command_prefix = '!'
modules = []

for f in glob('scripts/script_*.py'):
    module_name = f.replace('/', '.').replace('.py', '')
    module = import_module(module_name, package=__name__)
    try:
        module.init()
    except Exception:
        pass
    modules.append(module)


def _pp(o):
    parsed = json.loads(o.to_json())
    print(json.dumps(parsed, indent=4, sort_keys=True))


def _handle_module(update, context, module):
    message = update.message
    text = ''
    if message and message.text:
        text = message.text

    if module.COMMAND is None or text.startswith(
            f'{command_prefix}{module.COMMAND}'):
        module.handle(update, context)


def message(update, context):
    for module in modules:
        try:
            _handle_module(update, context, module)
        except Exception as e:
            return_error = str(e)
            if return_error:
                update.message.reply_text(return_error)

handler = MessageHandler(filters=Filters.all, callback=message)
updater.dispatcher.add_handler(handler)

updater.start_polling()
updater.idle()

for module in modules:
    try:
        module.exit()
    except Exception:
        pass
