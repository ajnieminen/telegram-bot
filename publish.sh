#!/bin/bash

rsync -av requirements.txt 167.172.176.237:/home/ajnieminen/python/bot/requirements.txt
rsync -av bot.py 167.172.176.237:/home/ajnieminen/python/bot/bot.py
rsync -av cron.py 167.172.176.237:/home/ajnieminen/python/bot/cron.py
rsync -av scripts/ 167.172.176.237:/home/ajnieminen/python/bot/scripts/ --exclude="__pycache__"
rsync -av images/ 167.172.176.237:/home/ajnieminen/python/bot/images/
