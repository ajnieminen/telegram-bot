import pathlib
import re
import string

try:
    from libvoikko import Voikko
except:
    pass


DATA = {}
MAX_MESSAGES = 1000
WORD_COUNT = 10

def _get_message_array(chat):
    arr = DATA[chat.id] if chat.id in DATA else []
    return arr[-MAX_MESSAGES:]

def _trim_words(words_array):
    trim_lmb = lambda x: len(x) > 3
    return list(filter(trim_lmb, words_array))

def _trim_text(text):
    text = text.lower()
    text = text.replace('.', ' ')
    text = text.replace(',', ' ')
    text = text.replace('?', ' ')
    text = text.replace(':', '')
    text = text.replace(';', '')
    text = text.replace('(', '')
    text = text.replace(')', '')
    text = text.replace('[', '')
    text = text.replace(']', '')
    return text

def _handle_text(text):
    """ remove ulrs etc """
    print('text', text)
    if isinstance(text, str):
        return text
    if isinstance(text, list):
        print('text list', text)
        txt_arr = []
        for o in text:
            if isinstance(o, str):
                txt_arr.append(o)
        return ' '.join(txt_arr)

def _split_message(message):
    try:
        if len(message.entities) > 0:
            return []
        text = message.text
        text = _trim_text(text)
        return _trim_words(text.split(' '))
    except Exception as e:
        return []

def _add_word_to_dict(word_dict, word):
    count = 1
    if word in word_dict:
        count = word_dict[word] + 1
    word_dict[word] = count

def add_message(chat, message):
    message_array = _get_message_array(chat)
    message_array.append(message)
    DATA[chat.id] = message_array

def _get_valid_word(voikko, word, arguments):
    if voikko is None or arguments.words.lower() == 'all':
        return word
    try:
        voikko_dict = voikko.analyze(word)
        ignored_classes = ['huudahdussana', 'sidesana',
                           'seikkasana', 'asemosana']
        word_class = voikko_dict[0]['CLASS']
        word_baseform = voikko_dict[0]['BASEFORM']
        if word_class in ignored_classes:
            return None
        if arguments.baseform == 1:
            return word_baseform
        return word
    except:
        return None

def _init_voikko():
    dir = pathlib.Path(__file__).parent.parent.absolute()
    print('init voikko from dir', dir)
    return Voikko(u'fi', path=f'{dir}/voikko/')

def handle_show_digest(chat, message, arguments):
    v = None
    try:
        v = _init_voikko()
    except Exception as e:
        print('voikko e', e)
    word_dict = {}
    for msg in _get_message_array(chat):
        for word in _split_message(msg):
            valid = _get_valid_word(v, word, arguments)
            if valid is not None:
                _add_word_to_dict(word_dict, valid)
    reply_text = ''
    sorted_dict = sorted(word_dict.items(), key=lambda x: x[1], reverse=True)
    for word, value in sorted_dict[:WORD_COUNT]:
        reply_text += f'{value} {word}\n'
    message.reply_text(f'Tiivistelmä:\n{reply_text}')
