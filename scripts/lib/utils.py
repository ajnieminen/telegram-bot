def _get_message_user_chat(update):

    try:
        message = update.effective_message
        user = update.effective_user
        chat = update.effective_chat
    except Exception:
        raise Exception()

    if message is None or user is None or chat is None:
        raise Exception()

    return message, user, chat


def _get_message_text(message):
    if message.text is not None:
        return message.text
    else:
        return ''

def _get_username(update):
    user = update.effective_user
    if user is not None:
        if user.username:
            return user.username
        else:
            return user.full_name
    return '-'
