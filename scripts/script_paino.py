#!/bin/python

from peewee import SqliteDatabase, \
    Model, CharField, DateTimeField, \
    DoubleField, IntegerField
from datetime import datetime as dt

from pydash import py_
from pydash import flatten_deep
from pydash import push
from pydash import is_empty
from pydash import filter_
from pydash import chunk

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

from matplotlib.dates import bytespdate2num, num2date
from matplotlib.ticker import Formatter

import numpy as np
import arrow
import pprint
import json

import os

NAME = 'paino'
COMMAND = 'paino'

HELP_TEXT = '''

!paino <kg>
!paino <YYYY-MM-DD h:m> <kg>

Poista paino syöttämällä painoksi 0.

Voit korvata vanhan paino syöttämällä uuden
30 minutin sisään vanhasta.
'''

db = SqliteDatabase('paino.db')


class Paino(Model):
    channel_id = IntegerField()
    user_id = IntegerField()
    username = CharField()
    datetime = DateTimeField()
    weight = DoubleField()
    created_date = DateTimeField(default=dt.now)

    class Meta:
        database = db

    def __str__(self):
        return f'{self.user_id} {self.username} {self.datetime} {self.weight}'


def _create_tables():
    with db:
        db.create_tables([Paino])


def init():
    _create_tables()


def _parse_weight_str(weight_str):
    try:
        return float(weight_str.replace(',', '.'))
    except:
        raise Exception('invalid weight, try numbers')


def _parse_date(date_str, fmt='YYYY-MM-DD h:m'):
    try:
        return arrow.get(date_str, fmt)
    except:
        raise Exception(f'invalid date format, try {fmt}')


def _get_message_text(message):
    if message.text is not None:
        return message.text
    else:
        return ''


def _parse_parameters(update, message):
    parameters = _get_message_text(message).split(' ')
    if len(parameters) == 1:
        update.message.reply_text(HELP_TEXT)
        raise Exception()
    elif len(parameters) == 2:
        return {
            'datetime': arrow.get(),
            'weight': _parse_weight_str(parameters[1])
        }
    elif len(parameters) == 4:
        return {
            'datetime': _parse_date(f'{parameters[1]} {parameters[2]}'),
            'weight': _parse_weight_str(parameters[3])
        }
    else:
        raise Exception('error parsin parameters')


def _get_username(update):
    user = update.effective_user
    if user is not None:
        if user.username:
            return user.username
        else:
            return user.full_name
    return '-'


def _print_update(update):
    pprint.pprint(json.loads(update.to_json()))


def _get_message_user_chat(update):

    try:
        message = update.effective_message
        user = update.effective_user
        chat = update.effective_chat
    except Exception:
        raise Exception()

    if message is None or user is None or chat is None:
        raise Exception()

    return message, user, chat


def _append_date_query(query, datetime, daysbefore):
    query.where()


def _get_painos_by_day(user_id, datetime, count=1):
    try:
        return Paino.select().where(
            Paino.user_id == user_id, Paino.datetime.year == datetime.year(),
            Paino.datetime.month == datetime.mont(),
            Paino.datetime.day == datetime.day()).order_by(
                Paino.datetime).limit(count)
    except Exception:
        return []


def _get_days_difference_in_painos(paino1, paino2):
    try:
        a1 = arrow.get(paino1.datetime).date()
        a2 = arrow.get(paino2.datetime).date()
        days = (a1 - a2).days
        log(f'dates difference {a1} - {a2}, is {days} days')
        return days
    except Exception as e:
        log(f'dates differences exeption {paino1} {paino2}, {e}')
        return 0


def log(message, level=0, padding=0):
    print('message', message)


def _get_user_stat_painos(user_id):
    latest_paino = _get_latest_paino(user_id)

    dt = arrow.get(latest_paino.datetime).shift(months=-1).datetime
    got_paino = Paino.select().where(
        Paino.user_id == user_id,
        Paino.datetime > dt).order_by(-Paino.datetime)

    days_dict = {}
    for p in got_paino:
        try:
            d = _get_days_difference_in_painos(latest_paino, p)
            if d > 0 and d not in days_dict:
                days_dict[d] = p
            elif d > 0 and d in days_dict and days_dict[d].weight > p.weight:
                days_dict[d] = p
        except Exception as e:
            log(f'days dict exception {e}')

    if is_empty(days_dict):
        return []

    filtered = {}
    for i in [1, 7, 14, 30]:
        sup = min(days_dict, key=lambda x: abs(x - i))
        filtered[sup] = days_dict[sup]

    return list(filtered.values())


def _get_user_stats(user_id):
    latest_paino = _get_latest_paino(user_id)

    if is_empty(latest_paino):
        return []

    stats = [f'viimeisin paino: {latest_paino.weight}']
    painos = []
    push(painos, _get_user_stat_painos(user_id))
    push(painos, _get_first_paino(user_id))
    painos = flatten_deep(painos)
    painos = filter_(painos, lambda x: x is not None)
    days_and_weights = {}

    for paino in painos:
        log(f'paino in painos: {paino}')
        days_difference = _get_days_difference_in_painos(latest_paino, paino)

        if days_difference < 1:
            continue

        weight_change = -(paino.weight - latest_paino.weight)
        days_and_weights[days_difference] = round(weight_change, 3)

    for day, weight in days_and_weights.items():
        stats.append(f'muutos {day} päivässä: {weight}')

    return stats


def _get_latest_paino(user_id):
    try:
        p = Paino.select().where(
            Paino.user_id == user_id).order_by(-Paino.datetime).get()
        log(f'_get_latest_paino for user_id {user_id} -> {p}')
        return p
    except Exception:
        log(f'_get_latest_paino for user_id {user_id} -> None')
        return None


def _get_first_paino(user_id):
    try:
        paino = Paino.select().where(Paino.user_id == user_id).order_by(
            Paino.datetime).get()
        log(f'get_first_paino: {paino}')
    except Exception:
        log('get_first_paino: None')
        return None


def _get_painos_in_range(user, datetime):
    uprange = datetime.shift(minutes=-30)
    downrange = datetime.shift(minutes=30)
    return Paino.select().where(Paino.user_id == user.id,
                                Paino.datetime >= uprange.datetime,
                                Paino.datetime <= downrange.datetime)


def _delete_painos_in_range(user, datetime):
    for paino in _get_painos_in_range(user, datetime):
        paino.delete_instance()


def _insert_new_paino(update, input_values):
    print('inset new paino', input_values)

    message, user, chat = _get_message_user_chat(update)

    try:

        return_strings = []

        current_weight = input_values['weight']
        datetime = input_values['datetime']

        _delete_painos_in_range(user, datetime)

        if (current_weight > 0):
            Paino.create(channel_id=chat.id,
                         user_id=user.id,
                         username=_get_username(update),
                         datetime=datetime.datetime,
                         weight=current_weight)
            return_strings.append('Paino lisätty')

    except Exception as e:
        print('insert new paino exception', e)
        pass


def _parse_date_weight(arguments):
    return {
        'datetime': _parse_date(f'{arguments[1]} {arguments[2]}'),
        'weight': _parse_weight_str(arguments[3])
    }


def _parse_weight(arguments):
    return {'datetime': arrow.get(), 'weight': _parse_weight_str(arguments[1])}


def _parse_input_values(arguments):
    try:
        return _parse_date_weight(arguments)
    except Exception:
        pass

    try:
        return _parse_weight(arguments)
    except Exception:
        pass

    raise Exception('virheellinen viesti?')


def _send_user_painos_message(update):
    user = update.effective_user
    painos = Paino.select().where(
        Paino.user_id == user.id).order_by(-Paino.datetime)

    for p in chunk(painos, 20):
        str = ''
        for paino in p:
            str = f'{str}{paino.datetime}: {paino.weight}\n'
        try:
            user.send_message(str)
        except Exception as e:
            print('e', e)
        print('sent painolist message to user', user.id)


def _filter_painos_by_date(painos, lowest_paino=True):
    '''
    TODO there must be a better way!
    '''
    return_painos = {}
    for paino in painos:
        paino_date = arrow.get(paino.datetime).date()
        if paino_date not in return_painos:
            return_painos[paino_date] = paino
        else:
            return_paino = return_painos[paino_date].weight
            if lowest_paino is True and return_paino > paino.weight:
                return_painos[paino_date] = paino
            elif lowest_paino is False and return_paino < paino.weight:
                return_painos[paino_date] = paino
    return list(return_painos.values())


def _get_painos_between_dates(user_id,
                              first_date,
                              last_date,
                              one_by_day=True,
                              lowest_paino=True):
    log(f'getting painos between dates {user_id}, {first_date} - {last_date}')
    try:
        painos = Paino.select().where(
            Paino.user_id == user_id,
            Paino.datetime >= first_date).order_by(-Paino.datetime)
        if one_by_day:
            painos = _filter_painos_by_date(painos)
        return painos
    except Exception:
        return []


def _dates_equals(paino, date):
    return arrow.get(paino.datetime).date() == date


def _get_weight_chart(update):
    user = update.effective_user
    log(f'getting weight chart for user {user.id}')

    date_data = []
    paino_data = []

    now_date = arrow.now()
    painos = _get_painos_between_dates(
        user_id=user.id,
        first_date=now_date.shift(days=-365).date(),
        last_date=now_date.date())

    if len(painos) == 0:
        with open(f'images/obese.jpg', 'rb') as f:
            update.message.reply_photo(f)
        return

    for paino in painos:
        datestr = arrow.get(paino.datetime).date()
        date_data.append(datestr)
        paino_data.append(paino.weight)

    fig, ax = plt.subplots()
    line1, = ax.plot(date_data,
                     paino_data,
                     linestyle='-',
                     marker=',',
                     label=f'{user.full_name}')
    fig.subplots_adjust(bottom=0.2)
    plt.xticks(rotation=90)
    plt.grid(True)
    ax.legend()

    if not os.path.exists("images"):
        os.mkdir("images")
    plt.savefig(f'images/{user.id}.png')

    with open(f'images/{user.id}.png', 'rb') as f:
        update.message.reply_photo(f)


def _has_argument_command(arguments, command):
    try:
        return arguments[1] == command
    except Exception:
        return False


def _handle_input(update, arguments, message, user, chat):
    # tavoite = aseta tavoite päivämäärä sekä paino
    # pituus / bmi laskuri
    if len(arguments) == 1:
        text = ''
        try:
            stats = _get_user_stats(user.id)
            text = '\n'.join(stats)
        except Exception:
            pass
        text = text + ' ' + HELP_TEXT
        update.message.reply_text(text)
        raise Exception()  # this is fine
    elif _has_argument_command(arguments, 'listaa'):
        try:
            _send_user_painos_message(update)
        except Exception:
            pass
    elif _has_argument_command(arguments, 'kaavio'):
        try:
            _get_weight_chart(update)
        except Exception as e:
            print('e', e)
            pass
    else:
        input_values = _parse_input_values(arguments)
        pprint.pprint(input_values)
        _insert_new_paino(update, input_values)
        stats = _get_user_stats(user.id)
        str = '\n'.join(stats)
        update.message.reply_text(f'paino lisätty. \n {str}')


def handle(update, context):
    print('handling update', update)
    message, user, chat = _get_message_user_chat(update)
    arguments = _get_message_text(message).split(' ')
    print('handling input', arguments)
    _handle_input(update, arguments, message, user, chat)
