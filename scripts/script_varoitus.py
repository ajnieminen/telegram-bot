#!/bin/python

import pprint
import json

from peewee import SqliteDatabase, \
    Model, CharField, DateTimeField, \
    IntegerField, fn
from datetime import datetime as dt

import matplotlib
import matplotlib.pyplot as plt

import os

import numpy as np

NAME = 'varoitus'
COMMAND = 'varoitus'

HELP_TEXT = '''
'''

db = SqliteDatabase('varoitus.db')

pp = pprint.PrettyPrinter(indent=4)

VIRHE_LIMIT = 3
IS_TEST = False

MESSAGE_TYPE_NORMAL = 0
MESSAGE_TYPE_REPLY = 1


class Varoitus(Model):
    channel_id = IntegerField()
    user_id = IntegerField()
    from_user_id = IntegerField()
    message_id = IntegerField()
    varoitus_count = IntegerField()
    datetime = DateTimeField()
    username = CharField()
    text = CharField()
    comment = CharField()
    created_date = DateTimeField(default=dt.now)

    class Meta:
        database = db

    def __str__(self):
        return f'{self.user_id} {self.username} {self.datetime}'


class Virhe(Model):
    channel_id = IntegerField()
    user_id = IntegerField()
    username = CharField()
    message_id = IntegerField()
    varoitus_count = IntegerField()
    created_date = DateTimeField(default=dt.now)

    class Meta:
        database = db

    def __str__(self):
        return f'{self.user_id} {self.varoitus_count}'


class MessageData(Model):
    message_id = IntegerField()
    message_typ = IntegerField()
    json = CharField()

    class Meta:
        database = db

    def __str__(self):
        return f'{self.message_id} {self.message_type}'


def _pp(o):
    parsed = json.loads(o.to_json())
    print(json.dumps(parsed, indent=4, sort_keys=True))


def _create_tables():
    with db:
        db.create_tables([Varoitus, Virhe, MessageData])


def init():
    _create_tables()


def _get_username(user):
    if user is not None:
        if user.username:
            return user.username
        else:
            return user.full_name
    return '-'


def _get_varoitus_count(user_id):
    return Varoitus.select().where(Varoitus.user_id == user_id).count()


def _user_check(update):
    if IS_TEST:
        return True
    user = update.message.from_user
    reply_user = update.message.reply_to_message.from_user
    if reply_user.is_bot or user.id == reply_user.id:
        return False
    return True


def _get_comment(message):
    try:
        return message.text.split(' ', 1)[1]
    except Exception:
        return ''


def _varoitus_exists(update):
    from_user_id = update.message.from_user.id
    user_id = update.message.reply_to_message.from_user.id
    if IS_TEST:
        user_id = 0
    message_id = update.message.reply_to_message.message_id
    count = Varoitus.select().where(Varoitus.from_user_id == from_user_id,
                                    Varoitus.user_id == user_id,
                                    Varoitus.message_id == message_id).count()
    return count > 0


def _add_varoitus(update, message):
    if not _user_check(update):
        raise Exception('nah brah')
    username = _get_username(message.from_user)
    varoitus_count = _get_varoitus_count(message.from_user.id)
    comment = _get_comment(update.message)
    text = message.text if message.text else ''
    from_user_id = update.message.from_user.id
    return Varoitus.create(channel_id=message.chat.id,
                           user_id=message.from_user.id,
                           from_user_id=from_user_id,
                           message_id=message.message_id,
                           varoitus_count=varoitus_count,
                           datetime=message.date,
                           username=username,
                           text=text,
                           comment=comment)


def _add_virhe(varoitus, varoitus_count):
    try:
        virhe = Virhe.select().where(
            Virhe.channel_id == varoitus.channel_id,
            Virhe.user_id == varoitus.user_id,
            Virhe.message_id == varoitus.message_id).get()
        virhe.varoitus_count = varoitus_count
        virhe.save()
        return virhe
    except Exception:
        return Virhe.create(channel_id=varoitus.channel_id,
                            user_id=varoitus.user_id,
                            username=varoitus.username,
                            message_id=varoitus.message_id,
                            varoitus_count=varoitus_count)


def _check_virhe(varoitus):
    varoitus_count = Varoitus.select().where(
        Varoitus.user_id == varoitus.user_id,
        Varoitus.channel_id == varoitus.channel_id,
        Varoitus.message_id == varoitus.message_id).count()
    if varoitus_count > (VIRHE_LIMIT - 1):
        _add_virhe(varoitus, varoitus_count)
        if varoitus_count == VIRHE_LIMIT:
            return True
    return False


def _get_stats(channel_id):
    init_dict = {'un': '', 'gw': 0, 'tw': 0, 'ue': 0}
    userstats = {}
    for v in Varoitus.select().where(Varoitus.channel_id == channel_id):
        if v.user_id not in userstats:
            userstats[v.user_id] = init_dict.copy()
        if v.from_user_id not in userstats:
            userstats[v.from_user_id] = init_dict.copy()

        userstats[v.user_id]['un'] = v.username
        userstats[v.user_id]['tw'] = userstats[v.user_id]['tw'] + 1
        userstats[v.from_user_id]['gw'] = userstats[v.from_user_id]['gw'] + 1

    for e in Virhe.select().where(Virhe.channel_id == channel_id):
        if e.user_id not in userstats:
            userstats[e.user_id] = init_dict.copy()
        userstats[e.user_id]['ue'] = userstats[e.user_id]['ue'] + 1

    usernames = []
    given_warnings = []
    taken_warnings = []
    user_errors = []

    for u in userstats:
        us = userstats[u]
        if us['un'] != '':
            usernames.append(us['un'])
            given_warnings.append(us['gw'])
            taken_warnings.append(us['tw'])
            user_errors.append(us['ue'])

    return usernames, given_warnings, taken_warnings, user_errors


def _autolabel(ax, rects):
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 3, height),
                    xytext=(0, 3),
                    textcoords="offset points",
                    ha='center',
                    va='bottom')


def _get_stats_chart(update):
    channel_id = update.message.chat.id
    usernames, given_warnings, taken_warnings, user_errors = _get_stats(
        channel_id=channel_id)

    x = np.arange(len(usernames))
    width = 0.3

    fig, ax = plt.subplots()
    fig.set_size_inches(18.5, 10.5)
    rects1 = ax.bar(x - width / 2,
                    given_warnings,
                    width,
                    label='Annetut varoitukset')
    rects2 = ax.bar(x + width / 2,
                    taken_warnings,
                    width,
                    label='Saadut varoitukset')
    rects3 = ax.bar(x + (width * 1.5),
                    user_errors,
                    width,
                    label='Saadut virheet')

    ax.set_ylabel('')
    ax.set_title('')
    ax.set_xticks(x)
    ax.set_xticklabels(usernames)
    ax.legend()

    _autolabel(ax, rects1)
    _autolabel(ax, rects2)
    _autolabel(ax, rects3)

    fig.tight_layout()

    if not os.path.exists("images"):
        os.mkdir("images")
    plt.savefig(f'images/varoitukset.png', dpi=200)

    with open(f'images/varoitukset.png', 'rb') as f:
        update.message.reply_photo(f)


def _print_varoitukset(update):
    query = (Virhe.select(Virhe.user_id, Virhe.username,
                          fn.COUNT(Virhe.user_id).alias('count')).group_by(
                              Virhe.user_id).order_by(
                                  fn.COUNT(Virhe.user_id).desc()))
    reply_str = ''
    for v in query:
        reply_str += f'{v.username} varoitukset: {v.count} \n'

    if reply_str != '':
        update.message.reply_text(reply_str)


def _handle_message_data(message):
    pass


def handle(update, context):
    message = update.message

    if message.reply_to_message is not None:
        if not _varoitus_exists(update):
            varoitus = _add_varoitus(update, message.reply_to_message)
            if _check_virhe(varoitus):
                update.message.reply_text(
                    'Virhe rekisteröity! Nyt tarkkana pojat.')
    else:
        # _print_varoitukset(update)
        _get_stats_chart(update)
