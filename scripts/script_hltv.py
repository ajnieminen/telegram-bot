from peewee import SqliteDatabase, \
    Model, CharField, DateTimeField, \
    DoubleField, IntegerField
from peewee import ManyToManyField
from peewee import DoesNotExist
from peewee import fn

from datetime import datetime as dt

from requests import get
from bs4 import BeautifulSoup

from pydash import py_
from pydash import flatten_deep
from pydash import push
from pydash import is_empty
from pydash import filter_
from pydash import chunk

import numpy as np
import arrow
import pprint
import json

import sys
import os
import threading
import time

from .lib.utils import _get_message_user_chat
from .lib.utils import _get_message_text

NAME = 'hltv'
COMMAND = 'hltv'

HELP_TEXT = '''

!hltv <team_name>
'''

db = SqliteDatabase('hltv.db')

UPDATE_INTERVAL = 60*30
NOTIFY_TIME = 60*60*1
NOTIFY_LIMIT = NOTIFY_TIME + 60*30

class Notify(Model):

    chat_id = IntegerField()
    team_name = CharField()
    username = CharField()
    notified = DateTimeField()

    class Meta:
        database = db

    def __str__(self):
        return f'{self.chat_id} {self.team_name} {self.username} {self.notified}'

class Settings(Model):

    key = CharField(unique=True)
    value = CharField()

    class Meta:
        database = db

    def __str__(self):
        return f'{self.key} {self.value}'


class Team(Model):

    id = IntegerField(unique=True)
    name = CharField()

    class Meta:
        database = db

    def __str__(self):
        return f'{self.id} {self.name}'


class Match(Model):

    id = IntegerField(unique=True)
    url = CharField()
    meta = CharField()
    teams = ManyToManyField(Team, backref='matches')
    datetime = DateTimeField()
    created_date = DateTimeField(default=dt.now)

    class Meta:
        database = db

    def __str__(self):
        return f'{self.datetime} {self.teams}'


MatchTeams = Match.teams.get_through_model()


def get_matches():

    url = 'https://www.hltv.org/matches'
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    page = get(url, headers=headers)
    print('page', page)
    soup = BeautifulSoup(page.content, 'html.parser')

    matches = soup.find_all('div', class_='upcomingMatch')

    for match in matches:

        if 'team1' not in match.attrs:
            continue

        unix_timestamp = int(match.find('div', class_='matchTime')['data-unix'])
        date = dt.fromtimestamp(unix_timestamp / 1000)
        team_names = match.find_all('div', class_='matchTeamName')
        url = match.find('a', class_='match')['href']
        meta = match.find('div', class_='matchMeta').getText()

        yield {
            'id': int(url.split('/')[2]),
            'url': url,
            'datetime': date,
            'meta': meta,
            'team_ids': [match['team1'], match['team2']],
            'team_names': [n.get_text() for n in team_names]
        }

def get_team_by_name(name):
    return Team.get(fn.LOWER(Team.name) == name.lower())


def insert_match(match):
    team_data = list(zip(match['team_ids'], match['team_names']))
    for team in team_data:
        Team.insert(id=team[0],
                    name=team[1])\
            .on_conflict('replace')\
            .execute()
    Match.insert(id=match['id'],
                 url=match['url'],
                 meta=match['meta'],
                 datetime=match['datetime'])\
         .on_conflict('replace')\
         .execute()
    match = Match.get(Match.id == match['id'])
    for team in team_data:
        match.teams.add(Team.get(Team.id == team[0]))

def update_matches():
    print('update matches')
    for match in get_matches():
        try:
            Match.get(Match.id == match['id'])
        except DoesNotExist:
            print(' insert new match ', match)
            insert_match(match)

def _create_tables():
    try:
        with db:
            db.create_tables([Settings,
                              Team,
                              Match,
                              MatchTeams,
                              Notify])
    except Exception as e:
        pass

def cron():
    _create_tables()

    try:
        handle_cron_update()
    except Exception as e:
        print('e', e)

    try:
        handle_cron_notify()
    except Exception as e:
        print('handle cron notify exception', e)


def get_update_timestamp():
    try:
        setting = Settings.get(Settings.key == 'update_time')
        return int(setting.value)
    except DoesNotExist:
        return 0


def get_current_timestamp():
    return int(time.time())


def handle_cron_update():
    if get_current_timestamp() - get_update_timestamp() > UPDATE_INTERVAL:
        print('updating matches')
        try:
            update_matches()
        except Exception as e:
            print('update_matches ex', e)

        Settings.insert(key='update_time',
                        value=str(int(time.time())))\
                .on_conflict('replace')\
                .execute()

def add_to_notify_map(notify_map, notify, team):

    chat_id = notify.chat_id
    team_id = team.id
    username = notify.username

    if chat_id not in notify_map:
        notify_map[chat_id] = {}

    if team_id not in notify_map[chat_id]:
        notify_map[chat_id][team_id] = []

    if username not in notify_map[chat_id][team_id]:
        notify_map[chat_id][team_id].append(username)


def handle_cron_notify():

    print('handling cron notify')

    notify_time = arrow.now().shift(seconds=-NOTIFY_LIMIT).datetime
    print('nt', notify_time)
    to_notify = Notify.select()\
                      .where(Notify.notified < notify_time)

    for n in Notify.select():
        print('n', n)

    for n in to_notify:
        print('tn', n)

    notify_map = {}
    for n in to_notify:
        try:
            team = get_team_by_name(n.team_name)
            add_to_notify_map(notify_map, n, team)
        except DoesNotExist:
            pass

    handle_notify_map(notify_map)
    for n in Notify.select():
        print(' no ', n)


def handle_notify_map(notify_map):
    print('notifymap', notify_map)

    for chat_id in notify_map:
        try:
            handle_notify_chat(chat_id, notify_map)
        except:
            pass

def handle_notify_chat(chat_id, notify_map):
    for team_id in notify_map[chat_id]:
        team = Team.get(Team.id == team_id)
        matches = team.matches.where(Match.datetime > arrow.now().datetime,
                                     Match.datetime < arrow.now().shift(seconds=NOTIFY_TIME).datetime)\
                              .order_by(Match.datetime)\
                              .limit(1)
        users = notify_map[chat_id][team_id]
        handle_notify_match(chat_id, matches[0], users)

def get_match_string(match):
    rows = [f'{match.teams[0].name} vs {match.teams[1].name} ({match.meta})',
            f'{match.datetime}',
            f'https://hltv.org{match.url}']
    return '\n'.join(rows)

def handle_notify_match(chat_id, match, users):
    print('handle notify match', chat_id, match, users)
  
    try:
        token = os.environ['tbot_api']
        chat_id = chat_id
        users_string = ''
        for user in users:
            users_string += f'@{user} '
        message = f'{get_match_string(match)} {users_string}'
        send_text = ''.join([f'https://api.telegram.org/bot{token}',
                            f'/sendMessage?chat_id={chat_id}',
                            f'&parse_mode=Markdown&text={message}'])
        response = get(send_text)

        Notify.update(notified=arrow.now().datetime)\
              .where(Notify.username.in_(users))\
              .execute()

    except Exception as e:
        print(e)



def init():
    _create_tables()

def handle(update, context):
    message, user, chat = _get_message_user_chat(update)
    arguments = _get_message_text(message).split(' ')
    if len(arguments) > 2 and arguments[1] == 'notify':
        team_name = _get_message_text(message).split(' ', 2)[2]
        try:
            team = get_team_by_name(team_name)
        except DoesNotExist:
            raise Exception(f'team {team_name} not found')
        try:
            notify = Notify.get(Notify.chat_id == chat.id,
                                Notify.username == user.username,
                                Notify.team_name == team_name)
            notify.delete().execute()
            update.message.reply_text(f'notify deleted, {team_name}')
        except DoesNotExist as e:
            Notify.create(chat_id=chat.id,
                          username=user.username,
                          team_name=team_name,
                          notified=arrow.now().shift(days=-1).datetime)
            update.message.reply_text(f'notify inserted, {team_name}')
        except Exception as e:
            print('ex', e)
    elif len(arguments) > 1:
        team_name = _get_message_text(message).split(' ', 1)[1]
        try:
            team = get_team_by_name(team_name)
            matches = team.matches\
                          .where(Match.datetime > arrow.now().datetime)\
                          .order_by(Match.datetime)\
                          .limit(1)
            for m in matches:
                text = get_match_string(m)
                update.message.reply_text(text)
        except DoesNotExist:
            raise Exception(f'no team found by {team_name}')
