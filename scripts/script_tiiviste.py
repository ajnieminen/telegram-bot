
import argparse

from .lib.utils import _get_message_user_chat
from .lib.utils import _get_message_text
from .lib import tiiviste_array

NAME = 'tiiviste'
COMMAND = None

HELP_TEXT = '''
'''


def init():
    pass

def handle(update, context):
    message, user, chat = _get_message_user_chat(update)
    arguments = _get_message_text(message).split(' ')

    if len(arguments) > 0 and arguments[0].startswith('!tiivis'):
        try:
            parser = argparse.ArgumentParser(add_help=False)
            parser.add_argument('-limit', default=0, dest='limit')
            parser.add_argument('-words', default='', dest='words')
            parser.add_argument('-baseform', default=1, dest='baseform')
            args = parser.parse_args(arguments[1:])
            tiiviste_array.handle_show_digest(chat, message, args)
        except:
            raise Exception('paskat argumentit.')
    else:
        tiiviste_array.add_message(chat, message)
