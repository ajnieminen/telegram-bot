
from peewee import SqliteDatabase, \
    Model, CharField, DateTimeField, \
    DoubleField, IntegerField
from datetime import datetime as dt

import arrow

from .lib.utils import _get_message_user_chat
from .lib.utils import _get_username
from .lib.utils import _get_message_text


NAME = 'slots'
COMMAND = None

HELP_TEXT = '''
'''


db = SqliteDatabase('slots.db')

roll_start_time = 8
warning_count = 1

class Warning(Model):
    channel_id = IntegerField()
    user_id = IntegerField()
    username = CharField()
    datetime = DateTimeField()

    class Meta:
        database = db

    def __str__(self):
        return f'{self.user_id} {self.username}'

class Roll(Model):
    channel_id = IntegerField()
    user_id = IntegerField()
    username = CharField()
    datetime = DateTimeField()
    emoji = CharField()
    value = IntegerField()

    class Meta:
        database = db

    def __str__(self):
        return f'{self.user_id} {self.username} {self.datetime} {self.emoji} {self.value}'

def _create_tables():
    with db:
        db.create_tables([Roll, Warning])

def init():
    _create_tables()

def _current_match_datetime():
    now = arrow.now()
    now_time = now.time()
    start_datetime = now.replace(hour=roll_start_time,
                             minute=0, second=0,
                             microsecond=0)
    start_time = start_datetime.time()
    if (now_time < start_time):
        start_datetime = start_datetime.shift(days=-1)
    return start_datetime.date()

def _insert_roll(message, chat, user, username, dice):

    Roll.create(channel_id=chat.id,
                user_id=user.id,
                username=username,
                datetime=message.date,
                emoji=dice.emoji,
                value=dice.value)

def _insert_warning(chat, user, username, message):

    print('inserting warning for user ', username)
    Warning.create(channel_id=chat.id,
                   user_id=user.id,
                   username=username,
                   datetime=message.date)

def _get_warnings(chat, dt, usergroup=False):
    # TODO FIX
    wngs = Warning.select()\
                  .where(Warning.channel_id==chat.id,
                         Warning.datetime>dt)\
                  .group_by(Warning.user_id)
    return wngs


def _check_roll(message, chat,
                user, username,
                dice, match_dt):
    latest_roll_user_id = None
    try:
        lr = Roll.select()\
                 .where(Roll.channel_id==chat.id,
                        Roll.datetime>match_dt)\
                 .order_by(-Roll.datetime)\
                 .limit(1)\
                 .get()
        latest_roll_user_id = lr.user_id
    except:
        pass

    if latest_roll_user_id == user.id:
        _insert_warning(chat, user, username, message)

    warnings = Warning.select()\
                      .where(Warning.channel_id==chat.id,
                             Warning.user_id==user.id,
                             Warning.datetime<match_dt)\
                      .count()

def handle(update, context):

    message, user, chat = _get_message_user_chat(update)
    arguments = _get_message_text(update.message).split(' ')
    dt = _current_match_datetime()

    if len(arguments) > 0 and arguments[0].startswith('!slot'):
        text = 'Slots:\n'
        for r in Roll.select():
            text += f'{r.user_id} {r.emoji} {r.value}\n'

        disqualified = []
        wngs = False
        for w in _get_warnings(chat, dt):
            if not wngs:
                wngs = True
                text += 'varoitukset:\n'
            text += f'{w.username}\n'
        update.message.reply_text(text)

    username = _get_username(update)
    dice = update.message.dice

    if not dice:
        return

    if dice.emoji == '🎰': # and dice.value == 64:
        _insert_roll(message, chat, user,
                     username, dice)
        _check_roll(message, chat, user,
                    username, dice, dt)
